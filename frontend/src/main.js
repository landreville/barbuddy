// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueScrollTo from 'vue-scrollto';
import Multiselect from 'vue-multiselect';
import VueAnalytics from 'vue-analytics';
import { store } from './store';
import BarchefApp from './App';
import router from './router';
import '../node_modules/@fortawesome/fontawesome';

Vue.config.devtools = true;
Vue.config.productionTip = false;

Vue.component('multiselect', Multiselect);
Vue.use(VueScrollTo);
Vue.use(VueAnalytics, {
  id: 'UA-58723120-3',
  router
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { BarchefApp },
  template: '<barchef-app></barchef-app>',
});
