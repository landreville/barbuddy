import Vue from 'vue';
import Router from 'vue-router';
import recipes from '@/components/recipes';
import recipe from '@/components/recipe';
import editrecipe from '@/components/editrecipe';
import pantry from '@/components/pantry';
import { APP_TITLE } from '@/lib/constants';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'recipes',
      component: recipes,
      meta: { title: APP_TITLE, breadcrumbs: [['Recipes', { name: 'recipes' }]] }
    },
    {
      path: '/recipes/:id/:recipeName',
      name: 'recipe',
      component: recipe,
      props: { default: true },
      meta: {
        title: r => r.params.recipeName,
        breadcrumbs: [
          ['Recipes', { name: 'recipes' }],
          r => [r.params.recipeName, { name: 'recipe', id: r.params.id, recipeName: r.params.recipeName }]
        ]
      }
    },
    {
      path: '/recipes/:id/:recipeName/edit',
      name: 'edit-recipe',
      component: editrecipe,
      props: { default: true },
      meta: {
        title: r => r.params.recipeName,
        breadcrumbs: [
          ['Recipes', { name: 'recipes' }],
          r => [r.params.recipeName, { name: 'recipe', id: r.params.id, recipeName: r.params.recipeName }],
          r => [`Edit ${r.params.recipeName}`, { name: 'edit-recipe', id: r.params.id, recipeName: r.params.recipeName }]
        ]
      }
    },
    {
      path: '/pantry',
      name: 'pantry',
      component: pantry,
      meta: {
        title: 'Pantry',
        breadcrumbs: [
          ['Pantry', { name: 'pantry' }]
        ]
      },
    }
  ]
});


router.beforeEach((to, from, next) => {
  let title = to.meta.title;

  if ({}.toString.call(title) === '[object Function]') {
    title = title(to);
  }
  if (title !== APP_TITLE) {
    title = `${APP_TITLE} — ${title}`;
  }
  document.title = title;
  next();
});

export default router;
