import axios from 'axios';
import { API_BASE_URL } from './constants';
import { sortIngredients } from './util';

// const BASE_URL = 'http://localhost:4000/';
const BASE_URL = API_BASE_URL;
const API_URL = `${BASE_URL}api/`;


export class AxiosProxy {

  constructor() {
    this.axios = axios.create({ baseURL: API_URL });
    this.jws = null;
    this.accessToken = null;
  }

  setAccessToken(accessToken) {
    this.accessToken = accessToken;
  }

  get(url, query) {
    let getUrl = url;
    if (query) {
      let params = new URLSearchParams();
      for (let key in query) {
        if (query.hasOwnProperty(key)) {
          let value = query[key];
          if (Array.isArray(value)) {
            for (let i = 0; i < value.length; i++) {
              let arrayValue = value[i];
              if (arrayValue) {
                params.append(`${key}[]`, arrayValue);
              }
            }
          } else if (value) {
            params.append(key, value);
          }
        }
      }
      getUrl = `${getUrl}?${params.toString()}`;
    }
    return this.axios.get(getUrl, this.axiosConfig()).then(resp => resp.data.data);
  }

  post(url, body, refresh) {
    if (refresh === false) {
      return this.axios.post(url, body, this.axiosConfig());
    }
    return this.axios.post(url, body, this.axiosConfig());
  }

  put(url, body) {
    return this.axios.put(url, body, this.axiosConfig());
  }

  axiosConfig() {
    // Not using axios.create because the auth header value may change over time.
    return { headers: this.makeAuthHeader() };
  }

  makeAuthHeader() {
    if (this.accessToken) {
      return { Authorization: `Bearer ${this.accessToken}` };
    }
    return {};
  }
}


export class ApiClient {
  constructor() {
    this.api = new AxiosProxy();

    this.recipeBaseUrl = 'recipes';
    this.ingredientsUrl = 'ingredients';
    this.catalogsUrl = 'catalogs';
    this.categoriesUrl = 'categories';
    this.vesselsUrl = 'vessels';
    this.pantryUrl = 'pantry';
    this.favouritesUrl = 'favourites';
  }

  getCatalogs() {
    return this.api.get(this.catalogsUrl);
  }

  getCategories() {
    return this.api.get(this.categoriesUrl);
  }

  async getPantry() {
    return this.api.get(this.pantryUrl);
  }

  async getFavourites() {
    return this.api.get(this.favouritesUrl);
  }

  async getIngredients() {
    return this.api.get(this.ingredientsUrl);
  }

  getVessels() {
    return this.api.get(this.vesselsUrl);
  }

  getRecipe(id) {
    let url = `${this.recipeBaseUrl}/${id}`;
    return this.api.get(url).then((data) => {
      data.recipeIngredients.sort(sortIngredients);
      return data;
    });
  }

  async getRecipes(query) {
    return this.api.get(this.recipeBaseUrl, query);
  }

  async putFavourites(favourites) {
    return this.api.put(this.favouritesUrl, favourites);
  }

  async putPantry(pantryItems) {
    return this.api.put(this.pantryUrl, pantryItems);
  }

  putRecipe(id, recipe, image) {
    return this.api.put(`${this.recipeBaseUrl}/${id}`, recipe).then(
      (resp) => {
        if (image) {
          this.putRecipeImage(id, image);
        }
        return resp;
      }
    );
  }

  putRecipeImage(id, image) {
    let data = new FormData();
    data.append('image_file', image);
    return this.api.put(`${this.recipeBaseUrl}/${id}/image/main`, data);
  }

  setAccessToken(accessToken) {
    this.api.setAccessToken(accessToken);
  }
}

export const API = new ApiClient();

