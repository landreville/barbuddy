export const API_BASE_URL = '/';
export const APP_TITLE = 'Bar Buddy';
export const DEFAULT_VESSEL_IMAGE = '/static/images/vessel/oldfashioned.png';
export const VESSEL_IMAGES = {
  Rocks: '/static/images/vessel/oldfashioned.png',
  Cocktail: '/static/images/vessel/coupe.png',
  Coupe: '/static/images/vessel/coupe.png',
  Martini: '/static/images/vessel/coupe.png',
  Highball: '/static/images/vessel/highball.png',
  Collins: '/static/images/vessel/highball.png',
  Snifter: DEFAULT_VESSEL_IMAGE,
  Hurricane: '/static/images/vessel/hurricane.png',
  'Old Fashioned': '/static/images/vessel/oldfashioned.png',
  Shot: DEFAULT_VESSEL_IMAGE,
  'Clear Plastic Bag': DEFAULT_VESSEL_IMAGE,
  Cordial: DEFAULT_VESSEL_IMAGE,
  Goblet: DEFAULT_VESSEL_IMAGE,
  Flute: '/static/images/vessel/coupe.png',
  'Copper Mug': DEFAULT_VESSEL_IMAGE,
  'Double Rocks': DEFAULT_VESSEL_IMAGE,
  'Nick And Nora': '/static/images/vessel/coupe.png',
  'Fancy Fizz': '/static/images/vessel/highball.png',
  Wine: '/static/images/vessel/coupe.png',
  'Julep Tin': DEFAULT_VESSEL_IMAGE,
  Pilsner: '/static/images/vessel/highball.png',
  Port: DEFAULT_VESSEL_IMAGE,
  'Coconut Mug': '/static/images/vessel/hurricane.png',
  'Large Coupe': '/static/images/vessel/coupe.png',
  'Tiki Mug': '/static/images/vessel/hurricane.png',
  Fizz: '/static/images/vessel/highball.png',
  Punch: '/static/images/vessel/highball.png'
};
