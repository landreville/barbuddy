import { DEFAULT_VESSEL_IMAGE, VESSEL_IMAGES } from './constants';

export function sortIngredients(a, b) {
  if (a.unit === 'ounce' && b.unit !== 'ounce') {
    return -1;
  } else if (b.unit === 'ounce' && a.unit !== 'ounce') {
    return 1;
  }

  if (a.unit !== b.unit) {
    if (a.unit === null) {
      return 1;
    } else if (b.unit === null) {
      return -1;
    }
    let x = a.unit.toLowerCase();
    let y = b.unit.toLowerCase();
    if (x < y) {
      return -1;
    } else if (x > y) {
      return 1;
    }
  }

  if (b.amount === a.amount) {
    let x = a.ingredientName.toLowerCase();
    let y = b.ingredientName.toLowerCase();
    if (x < y) {
      return -1;
    } else if (x > y) {
      return 1;
    }
  }

  return b.amount - a.amount;
}

export function photoUrl(recipe) {
  if (recipe.photoUrl) {
    return this.recipe.photoUrl;
  }

  if (recipe.vessel in VESSEL_IMAGES) {
    return VESSEL_IMAGES[recipe.vessel];
  }
  return DEFAULT_VESSEL_IMAGE;
}
