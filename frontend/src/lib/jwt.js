function decodeJWS(serializedJWS) {
  let parts = serializedJWS.split('.');
  let b64header = parts[0];
  let b64payload = parts[1];

  let header = JSON.parse(window.atob(b64header));
  let payload = JSON.parse(window.atob(b64payload));
  return { header, payload };
}


export class JWT {

  constructor(accessToken) {
    let jws = decodeJWS(accessToken);
    this.header = jws.header;
    this.payload = jws.payload;

    for (let key in this.payload) {
      if (this.payload.hasOwnProperty(key)) {
        let value = this.payload[key];
        this[key] = value;
      }
    }

    this.expDate = new Date(this.payload.exp * 1000);
  }

  isExpired() {
    let now = new Date();
    return this.expDate <= now;
  }
}
