import Vue from 'vue';
import Vuex from 'vuex';
import { storeConfig } from './store';

export * from './store';


Vue.use(Vuex);

export const store = new Vuex.Store(storeConfig);
