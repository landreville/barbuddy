import { API } from '../lib/apiclient';
import { JWT } from '../lib/jwt';

export const ACCESS_TOKEN = 'accessToken';
export const ADD_FAVOURITE = 'addFavourite';
export const CATALOGS = 'catalogs';
export const CATEGORIES = 'categories';
export const FAVOURITES = 'favourites';
export const IS_ADMIN = 'isAdmin';
export const FETCH_CATALOGS = 'fetchCatalogs';
export const FETCH_CATEGORIES = 'fetchCategories';
export const FETCH_FAVOURITES = 'fetchFavourites';
export const FETCH_INGREDIENTS = 'fetchIngredients';
export const FETCH_PANTRY = 'fetchPantryItems';
export const FETCH_RECIPES = 'fetchRecipes';
export const FETCH_VESSELS = 'fetchVessels';
export const INGREDIENT_OPTIONS = 'ingredientOptions';
export const INGREDIENTS = 'ingredients';
export const PANTRY_ITEMS = 'pantryItems';
export const RECIPES = 'recipes';
export const REMOVE_FAVOURITE = 'removeFavourite';
export const SEARCH = 'search';
export const SEARCH_RECIPES = 'searchRecipes';
export const SET_ACCESS_TOKEN = 'setAccessToken';
export const SET_PANTRY_ITEMS = 'setPantryItems';
export const SET_RECIPE = 'setRecipe';
export const SAVE_PANTRY = 'savePantry';
export const SAVE_RECIPE = 'saveRecipe';
export const VESSELS = 'vessels';

const ADD_RECIPE = 'addRecipe';
const SAVE_FAVOURITES = 'saveFavourites';
const SET_CATALOGS = 'setCatalogs';
const SET_CATEGORIES = 'setCategories';
const SET_FAVOURITES = 'setFavourites';
const SET_INGREDIENTS = 'setIngredients';
const SET_RECIPES = 'setRecipes';
const SET_SEARCH = 'setSearch';
const SET_VESSELS = 'setVessels';

// milliseconds
const cacheTime = 900000;
let lastUpdated = {};

function isCacheFresh(key) {
  if (lastUpdated.hasOwnProperty(key) && lastUpdated[key]) {
    let lastUpdate = lastUpdated[key];
    let now = new Date();
    if (now - lastUpdate < cacheTime) {
      return true;
    }
  }
  lastUpdated[key] = new Date();
  return false;
}

export const storeConfig = {
  state: {
    [ACCESS_TOKEN]: null,
    [CATALOGS]: [],
    [CATEGORIES]: [],
    [INGREDIENTS]: [],
    [PANTRY_ITEMS]: [],
    [RECIPES]: [],
    [SEARCH]: {},
    [VESSELS]: [],
    [FAVOURITES]: []
  },
  getters: {
    [INGREDIENT_OPTIONS](state) {
      let ingOptions = [];
      for (let i = 0; i < state[INGREDIENTS].length; i++) {
        ingOptions.push(state[INGREDIENTS][i].ingredientName);
      }
      return ingOptions;
    },
    isLoggedIn(state) {
      let accessToken = state.accessToken;
      if (accessToken) {
        let jwt = new JWT(accessToken);
        return !jwt.isExpired();
      }
      return false;
    },
    isAdmin(state) {
      let jwt = null;
      if (state.accessToken) {
        jwt = new JWT(state.accessToken);
      }

      if (jwt) {
        return jwt.admin === true;
      }
      return false;
    },
    username(state) {
      let accessToken = state.accessToken;
      if (accessToken) {
        let jwt = new JWT(accessToken);
        if (!jwt.isExpired()) {
          return jwt.name;
        }
      }
    },
    getJwt(state) {
      if (state.accessToken) {
        return new JWT(state.accessToken);
      }
      return null;
    }
  },
  mutations: {
    [ADD_FAVOURITE](state, recipeId) {
      state[FAVOURITES].push(recipeId);
    },
    [ADD_RECIPE](state, recipe) {
      state[RECIPES].push(recipe);
    },
    [REMOVE_FAVOURITE](state, recipeId) {
      let favs = state[FAVOURITES];
      for (let i = favs.length - 1; i >= 0; i--) {
        if (favs[i] === recipeId) {
          favs.splice(i, 1);
        }
      }
    },
    [SET_ACCESS_TOKEN](state, accessToken) {
      state[ACCESS_TOKEN] = accessToken;
      API.setAccessToken(accessToken);
      window.localStorage.setItem(ACCESS_TOKEN, JSON.stringify(accessToken));
    },
    [SET_CATALOGS](state, catalogs) {
      state[CATALOGS] = catalogs;
    },
    [SET_CATEGORIES](state, categories) {
      state[CATEGORIES] = categories;
    },
    [SET_FAVOURITES](state, favourites) {
      state[FAVOURITES] = favourites;
    },
    [SET_INGREDIENTS](state, ingredients) {
      state[INGREDIENTS] = ingredients;
    },
    [SET_PANTRY_ITEMS](state, pantryItems) {
      state[PANTRY_ITEMS] = pantryItems;
    },
    [SET_RECIPES](state, recipes) {
      state[RECIPES] = recipes;
    },
    [SET_SEARCH](state, search) {
      state[SEARCH] = search;
    },
    [SET_VESSELS](state, vessels) {
      state[VESSELS] = vessels;
    },
    [SET_RECIPE](state, { index, recipe }) {
      state[RECIPES][index] = recipe;
    }
  },
  actions: {
    async [ADD_FAVOURITE]({ state, dispatch, commit }, recipeId) {
      commit(ADD_FAVOURITE, recipeId);
      await dispatch(SAVE_FAVOURITES, state[FAVOURITES]);
    },

    async [FETCH_INGREDIENTS]({ commit }) {
      if (isCacheFresh(INGREDIENT_OPTIONS)) {
        return;
      }

      let ingredients = await API.getIngredients();
      commit(SET_INGREDIENTS, ingredients);
    },

    async [FETCH_CATALOGS]({ commit }) {
      if (isCacheFresh(CATALOGS)) {
        return;
      }

      commit(SET_CATALOGS, await API.getCatalogs());
    },

    async [FETCH_CATEGORIES]({ commit }) {
      if (isCacheFresh(CATEGORIES)) {
        return;
      }

      commit(SET_CATEGORIES, await API.getCategories());
    },

    async [FETCH_FAVOURITES]({ state, commit, getters }) {
      if (state[FAVOURITES].length > 0) {
        return;
      }

      let favs = [];
      if (getters.isLoggedIn) {
        favs = await API.getFavourites();
      } else {
        let localFavs = window.localStorage.getItem(FAVOURITES);
        if (localFavs) {
          favs = JSON.parse(localFavs);
        }
      }
      commit(SET_FAVOURITES, favs);
    },

    async [FETCH_PANTRY]({ state, commit, getters }) {
      if (state[PANTRY_ITEMS].length > 0) {
        return;
      }

      let pantryItems = [];
      if (getters.isLoggedIn) {
        pantryItems = await API.getPantry();
      } else {
        let localPantryItems = window.localStorage.getItem(PANTRY_ITEMS);
        if (localPantryItems) {
          pantryItems = JSON.parse(localPantryItems);
        }
      }

      commit(SET_PANTRY_ITEMS, pantryItems);
    },

    async [FETCH_RECIPES]({ state, commit }, { ignoreCache }) {
      if (!ignoreCache && isCacheFresh(RECIPES)) {
        return;
      }

      let recipes = await API.getRecipes(state.search);
      commit(SET_RECIPES, recipes);
    },

    async [FETCH_VESSELS]({ commit }) {
      if (isCacheFresh(VESSELS)) {
        return;
      }

      commit(SET_VESSELS, await API.getVessels());
    },

    async [REMOVE_FAVOURITE]({ state, dispatch, commit }, recipeId) {
      commit(REMOVE_FAVOURITE, recipeId);
      await dispatch(SAVE_FAVOURITES, state[FAVOURITES]);
    },

    async [SEARCH_RECIPES]({ commit, dispatch }, searchQuery) {

      commit(SET_SEARCH, searchQuery);

      if (searchQuery.pantry && searchQuery.pantry.length === 0) {
        commit(SET_RECIPES, []);
      } else {
        await dispatch(FETCH_RECIPES, { ignoreCache: true });
      }
    },

    async [SAVE_FAVOURITES]({ commit, getters }, favourites) {
      commit(SET_FAVOURITES, favourites);
      window.localStorage.setItem(FAVOURITES, JSON.stringify(favourites));
      if (getters.isLoggedIn) {
        await API.putFavourites(favourites);
      }
    },

    async [SAVE_PANTRY]({ commit, getters }, pantryItems) {
      commit(SET_PANTRY_ITEMS, pantryItems);
      window.localStorage.setItem(PANTRY_ITEMS, JSON.stringify(pantryItems));
      if (getters.isLoggedIn) {
        await API.putPantry(pantryItems);
      }
    },

    async [SAVE_RECIPE]({ state, commit }, { recipe, image }) {
      await API.putRecipe(recipe.recipeId, recipe, image);

      let found = false;
      for (let i = 0; i < state.recipes.length; i++) {
        let existingRecipe = state.recipes[i];
        if (existingRecipe.recipeId === recipe.recipeId) {
          found = true;
          commit(SET_RECIPE, { index: i, recipe });
        }
      }
      if (!found) {
        commit(ADD_RECIPE, recipe);
      }
    }
  }
};

