drop table fav_recipe;

create table fav_recipe(
  user_id varchar not null,
  recipe_id integer not null references recipe(recipe_id) on delete cascade on update cascade,
  constraint fav_recipe_pk primary key (user_id, recipe_id)
);

grant select, update, insert, delete on fav_recipe to barbuddy;
