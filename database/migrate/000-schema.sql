--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO postgres;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: appuser; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appuser (
    user_id integer NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    info jsonb DEFAULT '{}'::jsonb NOT NULL,
    preferences jsonb DEFAULT '{}'::jsonb NOT NULL,
    admin boolean DEFAULT false NOT NULL,
    pantry jsonb DEFAULT '[]'::jsonb NOT NULL,
    CONSTRAINT appuser_email_check CHECK (((email)::text <> ''::text)),
    CONSTRAINT appuser_password_check CHECK (((password)::text <> ''::text))
);


ALTER TABLE public.appuser OWNER TO postgres;

--
-- Name: appuser_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.appuser ALTER COLUMN user_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.appuser_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: catalog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.catalog (
    catalog character varying NOT NULL,
    CONSTRAINT catalog_catalog_check CHECK (((catalog)::text <> ''::text))
);


ALTER TABLE public.catalog OWNER TO postgres;

--
-- Name: fav_ingredient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fav_ingredient (
    user_id integer NOT NULL,
    ingredient_name character varying NOT NULL
);


ALTER TABLE public.fav_ingredient OWNER TO postgres;

--
-- Name: fav_recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fav_recipe (
    user_id integer NOT NULL,
    recipe_id integer NOT NULL
);


ALTER TABLE public.fav_recipe OWNER TO postgres;

--
-- Name: ingredient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ingredient (
    ingredient_name character varying NOT NULL,
    parent_ingredient_name character varying,
    description character varying,
    approved boolean DEFAULT false NOT NULL,
    CONSTRAINT ingredient_ingredient_name_check CHECK (((ingredient_name)::text <> ''::text))
);


ALTER TABLE public.ingredient OWNER TO postgres;

--
-- Name: pantry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pantry (
    user_id character varying NOT NULL,
    ingredient_name character varying NOT NULL
);


ALTER TABLE public.pantry OWNER TO postgres;

--
-- Name: recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe (
    recipe_name character varying NOT NULL,
    description character varying,
    catalog character varying,
    category character varying,
    directions text,
    story text,
    active boolean DEFAULT false NOT NULL,
    inserted_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    photo_path character varying,
    vessel character varying,
    recipe_id integer NOT NULL,
    CONSTRAINT recipe_recipe_name_check CHECK (((recipe_name)::text <> ''::text))
);


ALTER TABLE public.recipe OWNER TO postgres;

--
-- Name: recipe_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe_category (
    category character varying NOT NULL,
    CONSTRAINT recipe_category_category_check CHECK (((category)::text <> ''::text))
);


ALTER TABLE public.recipe_category OWNER TO postgres;

--
-- Name: recipe_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe_image (
    image_type character varying NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    mime_type character varying NOT NULL,
    image bytea NOT NULL,
    filename character varying,
    recipe_id integer NOT NULL,
    CONSTRAINT recipe_images_image_type_check CHECK (((image_type)::text = ANY (ARRAY[('main'::character varying)::text, ('thumbnail'::character varying)::text])))
);


ALTER TABLE public.recipe_image OWNER TO postgres;

--
-- Name: recipe_ingredient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe_ingredient (
    ingredient_name character varying NOT NULL,
    amount numeric,
    unit character varying,
    garnish boolean DEFAULT false NOT NULL,
    optional boolean DEFAULT false NOT NULL,
    recipe_id integer NOT NULL
);


ALTER TABLE public.recipe_ingredient OWNER TO postgres;

--
-- Name: recipe_recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipe_recipe_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipe_recipe_id_seq OWNER TO postgres;

--
-- Name: recipe_recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipe_recipe_id_seq OWNED BY public.recipe.recipe_id;


--
-- Name: substitute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.substitute (
    ingredient_name character varying NOT NULL,
    sub_ingredient_name character varying NOT NULL,
    ratio numeric DEFAULT 1 NOT NULL
);


ALTER TABLE public.substitute OWNER TO postgres;

--
-- Name: unit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unit (
    unit character varying NOT NULL,
    plural character varying,
    abbreviation character varying,
    decilitre_ratio numeric,
    CONSTRAINT unit_unit_check CHECK (((unit)::text <> ''::text))
);


ALTER TABLE public.unit OWNER TO postgres;

--
-- Name: vessel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vessel (
    vessel character varying NOT NULL,
    CONSTRAINT vessel_vessel_check CHECK (((vessel)::text <> ''::text))
);


ALTER TABLE public.vessel OWNER TO postgres;

--
-- Name: recipe recipe_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe ALTER COLUMN recipe_id SET DEFAULT nextval('public.recipe_recipe_id_seq'::regclass);


--
-- Name: appuser appuser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appuser
    ADD CONSTRAINT appuser_pkey PRIMARY KEY (user_id);


--
-- Name: catalog catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalog
    ADD CONSTRAINT catalog_pkey PRIMARY KEY (catalog);


--
-- Name: fav_ingredient fav_ingredient_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fav_ingredient
    ADD CONSTRAINT fav_ingredient_pk PRIMARY KEY (user_id, ingredient_name);


--
-- Name: ingredient ingredient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (ingredient_name);


--
-- Name: substitute ingredient_sub_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.substitute
    ADD CONSTRAINT ingredient_sub_pk PRIMARY KEY (ingredient_name, sub_ingredient_name);


--
-- Name: pantry pantry_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pantry
    ADD CONSTRAINT pantry_pk PRIMARY KEY (user_id, ingredient_name);


--
-- Name: recipe_category recipe_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_category
    ADD CONSTRAINT recipe_category_pkey PRIMARY KEY (category);


--
-- Name: recipe_image recipe_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_image
    ADD CONSTRAINT recipe_image_pkey PRIMARY KEY (recipe_id, image_type);


--
-- Name: recipe_ingredient recipe_ingredient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_ingredient
    ADD CONSTRAINT recipe_ingredient_pkey PRIMARY KEY (recipe_id, ingredient_name);


--
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (recipe_id);


--
-- Name: unit unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unit
    ADD CONSTRAINT unit_pkey PRIMARY KEY (unit);


--
-- Name: vessel vessel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vessel
    ADD CONSTRAINT vessel_pkey PRIMARY KEY (vessel);


--
-- Name: fav_recipe_recipe_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fav_recipe_recipe_id ON public.fav_recipe USING btree (recipe_id);


--
-- Name: ingredient_approved_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ingredient_approved_idx ON public.ingredient USING btree (approved);


--
-- Name: ingredient_parent_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ingredient_parent_idx ON public.ingredient USING btree (parent_ingredient_name);


--
-- Name: recipe_active_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipe_active_idx ON public.recipe USING btree (active);


--
-- Name: recipe_image_recipe_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipe_image_recipe_id ON public.recipe_image USING btree (recipe_id);


--
-- Name: recipe_ingredient_recipe_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipe_ingredient_recipe_id ON public.recipe_ingredient USING btree (recipe_id);


--
-- Name: fav_ingredient fav_ingredient_ingredient_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fav_ingredient
    ADD CONSTRAINT fav_ingredient_ingredient_name_fkey FOREIGN KEY (ingredient_name) REFERENCES public.ingredient(ingredient_name) ON UPDATE CASCADE;


--
-- Name: fav_ingredient fav_ingredient_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fav_ingredient
    ADD CONSTRAINT fav_ingredient_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.appuser(user_id) ON UPDATE CASCADE;


--
-- Name: fav_recipe fav_recipe_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fav_recipe
    ADD CONSTRAINT fav_recipe_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(recipe_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fav_recipe fav_recipe_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fav_recipe
    ADD CONSTRAINT fav_recipe_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.appuser(user_id) ON UPDATE CASCADE;


--
-- Name: ingredient ingredient_parent_ingredient_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingredient
    ADD CONSTRAINT ingredient_parent_ingredient_name_fkey FOREIGN KEY (parent_ingredient_name) REFERENCES public.ingredient(ingredient_name) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: pantry pantry_ingredient_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pantry
    ADD CONSTRAINT pantry_ingredient_name_fkey FOREIGN KEY (ingredient_name) REFERENCES public.ingredient(ingredient_name) ON UPDATE CASCADE;


--
-- Name: recipe recipe_catalog_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_catalog_fkey FOREIGN KEY (catalog) REFERENCES public.catalog(catalog) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: recipe recipe_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_category_fkey FOREIGN KEY (category) REFERENCES public.recipe_category(category) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: recipe_image recipe_image_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_image
    ADD CONSTRAINT recipe_image_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(recipe_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recipe_ingredient recipe_ingredient_ingredient_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_ingredient
    ADD CONSTRAINT recipe_ingredient_ingredient_name_fkey FOREIGN KEY (ingredient_name) REFERENCES public.ingredient(ingredient_name) ON UPDATE CASCADE;


--
-- Name: recipe_ingredient recipe_ingredient_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_ingredient
    ADD CONSTRAINT recipe_ingredient_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(recipe_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recipe_ingredient recipe_ingredient_unit_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_ingredient
    ADD CONSTRAINT recipe_ingredient_unit_fkey FOREIGN KEY (unit) REFERENCES public.unit(unit) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: recipe recipe_vessel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_vessel_fkey FOREIGN KEY (vessel) REFERENCES public.vessel(vessel) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: substitute substitute_ingredient_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.substitute
    ADD CONSTRAINT substitute_ingredient_name_fkey FOREIGN KEY (ingredient_name) REFERENCES public.ingredient(ingredient_name) ON UPDATE CASCADE;


--
-- Name: substitute substitute_sub_ingredient_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.substitute
    ADD CONSTRAINT substitute_sub_ingredient_name_fkey FOREIGN KEY (sub_ingredient_name) REFERENCES public.ingredient(ingredient_name) ON UPDATE CASCADE;


--
-- Name: TABLE appuser; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.appuser TO barbuddy;


--
-- Name: TABLE catalog; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.catalog TO barbuddy;


--
-- Name: TABLE fav_ingredient; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.fav_ingredient TO barbuddy;


--
-- Name: TABLE fav_recipe; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.fav_recipe TO barbuddy;


--
-- Name: TABLE ingredient; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.ingredient TO barbuddy;


--
-- Name: TABLE pantry; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.pantry TO barbuddy;


--
-- Name: TABLE recipe; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.recipe TO barbuddy;


--
-- Name: TABLE recipe_category; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.recipe_category TO barbuddy;


--
-- Name: TABLE recipe_image; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.recipe_image TO barbuddy;


--
-- Name: TABLE recipe_ingredient; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.recipe_ingredient TO barbuddy;


--
-- Name: TABLE substitute; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.substitute TO barbuddy;


--
-- Name: TABLE unit; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.unit TO barbuddy;


--
-- Name: TABLE vessel; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.vessel TO barbuddy;


--
-- PostgreSQL database dump complete
--

