alter table recipe
  add constraint recipe_name_catalog_uk unique(recipe_name, catalog);
