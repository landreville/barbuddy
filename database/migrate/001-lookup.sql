--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: recipe_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipe_category (category) FROM stdin;
Sour
Spirit-Forward
Highball
Hot
Flip
Tropical
\.


--
-- Data for Name: unit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unit (unit, plural, abbreviation, decilitre_ratio) FROM stdin;
ounce	ounces	oz	30
dash	dashes	\N	0.01
splash	splashes	\N	0.03
teaspoon	teaspoons	tsp	0.05
tablespoon	tablespoons	tbl	0.15
decilitre	decilitres	dl	1
slice	slices	\N	\N
leaf	leaves	\N	\N
sprig	sprigs	\N	\N
cube	cubes	\N	\N
drop	drops	\N	0.005
pinch	pinches	\N	\N
\.


--
-- Data for Name: vessel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vessel (vessel) FROM stdin;
Rocks
Cocktail
Coupe
Martini
Shot
Highball
Collins
Flute
Copper Mug
Snifter
Cordial
Goblet
Hurricane
Clear Plastic Bag
Old Fashioned
Double Rocks
Nick And Nora
Fancy Fizz
Old-Fashioned
Wine
Julep Tin
Pilsner
Port
Coconut Mug
Large Coupe
Tiki Mug
Fizz
Punch
\.


--
-- PostgreSQL database dump complete
--

