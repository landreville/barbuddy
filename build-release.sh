#!/bin/bash
set -e
set -x
set -o pipefail

RELEASE=

OPTIND=1
USAGE="$(basename "$0") [-h] [-r RELEASE] -- build docker images for barbuddy apps.

where:
    -h         show this help text
    -r RELEASE Tag images as this release."

while getopts "h?r:" opt; do
    case "${opt}" in
    h|\?)
        echo "${USAGE}"
        exit 0
        ;;
    r)  RELEASE="${OPTARG}"
        ;;
    esac
done

echo "Building images"
docker build -t registry.heyneat.ca/barbuddydb ./database
docker build -t registry.heyneat.ca/barbuddyapi ./backend
docker build -t registry.heyneat.ca/barbuddyfe ./frontend

echo "Pushing images"
docker push registry.heyneat.ca/barbuddydb
docker push registry.heyneat.ca/barbuddyapi
docker push registry.heyneat.ca/barbuddyfe

if [[ ! -z "${RELEASE}" ]]; then
    echo "Tagging release number."
    docker tag registry.heyneat.ca/barbuddydb registry.heyneat.ca/barbuddydb:${RELEASE}
    docker tag registry.heyneat.ca/barbuddyapi registry.heyneat.ca/barbuddyapi:${RELEASE}
    docker tag registry.heyneat.ca/barbuddyfe registry.heyneat.ca/barbuddyfe:${RELEASE}
    echo "Pushing release tags."
    docker push registry.heyneat.ca/barbuddydb:${RELEASE}
    docker push registry.heyneat.ca/barbuddyapi:${RELEASE}
    docker push registry.heyneat.ca/barbuddyfe:${RELEASE}
fi

if [[ ! -z "${RELEASE}" ]]; then
    echo "Tagging repo."
    hg tag -f "release-${RELEASE}"
fi
