import logging
import requests
from datetime import datetime
import pendulum
from jose import jwt as jose_jwt


log = logging.getLogger(__name__)


class GoogleSecretFetcher(object):

    _public_keys_url = (
        'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com'
    )
    _keys = None
    _expires = None

    def keys(self):
        if self._expires is not None:
            if self._expires > pendulum.now():
                return self._keys

        resp = requests.get(self._public_keys_url)
        self._expires = pendulum.from_format(
            resp.headers['expires'],
            'ddd, DD MMM YYYY HH:mm:ss z'
        )
        self._keys = resp.json()

        return self._keys

    def __getitem__(self, key_id):
        return self.keys()[key_id]


class JwtAttribute(object):

    def __init__(
        self,
        key_fetcher=None,
        verify_keys=None,
        issuer=None,
        audience=None,
        algorithms=None
    ):
        self.key_fetcher = key_fetcher
        self.verify_keys = verify_keys
        self.algorithms = algorithms
        self.issuer = issuer
        self.audience = audience

    def __call__(self, request):
        token = token_from_request(request)
        if token:
            return self._get_jwt_data(token)
        else:
            log.debug('No access token found in request.')

    def _get_jwt_data(self, token):
        try:
            data = jose_jwt.decode(
                token,
                key=self.key_fetcher.keys(),
                audience=self.audience,
                issuer=self.issuer,
                algorithms=self.algorithms
            )
        except jose_jwt.ExpiredSignatureError as e:
            log.info(f'JWT token is expired. {e}')
        except jose_jwt.JWTClaimsError as e:
            log.warning(f'JWT claims are invalid. {e}')
        else:
            return data


def token_from_request(request):
    auth_header = request.headers.get('Authorization')
    if auth_header:
        return auth_header[7:]

