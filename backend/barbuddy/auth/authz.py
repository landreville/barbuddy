import abc


class AuthorizationPolicy(object):

    def permits(self, context, principals, permission):
        if isinstance(permission, Permission):
            return permission.is_met(context, principals)
        else:
            return permission in principals

    def principals_allowed_by_permission(self, context, permission):
        raise NotImplemented


class Permission(object, metaclass=abc.ABCMeta):
    name = None

    @abc.abstractmethod
    def is_met(self, context, principals):
        pass


class Any(Permission):

    def __init__(self, *permissions):
        self._permissions = permissions

    def is_met(self, context, principals):
        for permission in self._permissions:
            if isinstance(permission, Permission):
                if permission.is_met(context, principals):
                    return True
            else:
                if permission in principals:
                    return True


class All(Permission):

    def __init__(self, *permissions):
        self._permissions = permissions

    def is_met(self, context, principals):
        for permission in self._permissions:
            if isinstance(permission, Permission):
                if not permission.is_met(context, principals):
                    return False
            else:
                if permission not in principals:
                    return False
        return True


class HasClaim(Permission):

    def __init__(self, claim, value):
        self.claim = claim
        self.value = value

    def is_met(self, context, principals):
        return self in principals

    def __eq__(self, other):
        return other.claim == self.claim and other.value == self.value

    def __hash__(self):
        return hash((self.claim, self.value))


class IsAdmin(Permission):

    def is_met(self, context, principals):
        jwt = context.request.jwt
        if jwt:
            return jwt.get('admin')


class Authenticated(Permission):

    def is_met(self, context, principals):
        return context.request.authenticated_userid is not None
