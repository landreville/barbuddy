import logging
from pyramid.settings import aslist
from .jwt import GoogleSecretFetcher, JwtAttribute
from .authn import JwtAuthenticationPolicy
from .authz import AuthorizationPolicy, HasClaim, IsAdmin, Authenticated

log = logging.getLogger(__name__)


def includeme(config):
    settings = config.get_settings()

    KEY_FETCHERS = {'Google': GoogleSecretFetcher}

    audience = settings.get('auth.jwt.audience')
    issuer = settings.get('auth.jwt.issuer')
    algorithms = aslist(settings.get('auth.jwt.algorithms'))

    key_fetcher_name = settings.get('auth.jwt.key_fetcher')
    if key_fetcher_name:
        key_fetcher = KEY_FETCHERS[key_fetcher_name]()
    else:
        key_fetcher = None

    verify_keys = {}
    for key, value in settings.items():
        if key.startswith('auth.jwt.key.'):
            verify_keys[key[13:]] = value

    attr = JwtAttribute(
        key_fetcher,
        verify_keys,
        issuer,
        audience,
        algorithms
    )

    config.add_request_method(attr, 'jwt', property=True, reify=True)
    config.set_authentication_policy(JwtAuthenticationPolicy())
    config.set_authorization_policy(AuthorizationPolicy())
