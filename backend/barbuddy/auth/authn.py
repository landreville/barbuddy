import logging
from jose import jwt as jose_jwt
from pyramid.security import Everyone, Authenticated
from .jwt import token_from_request
from .authz import HasClaim, IsAdmin

log = logging.getLogger(__name__)


class JwtAuthenticationPolicy(object):

    def remember(self, request, principal, **kw):
        raise NotImplemented

    def forget(self, request):
        raise NotImplemented

    def unauthenticated_userid(self, request):
        token = token_from_request(request)

        if token:
            return jose_jwt.get_unverified_claims(token)

    def authenticated_userid(self, request):
        return request.jwt['email']

    def effective_principals(self, request):
        principals = {Everyone}
        jwt = request.jwt
        if jwt:
            principals.add(Authenticated)
            if jwt['email_verified']:
                principals.add(HasClaim('email', jwt['email']))

            principals.add(HasClaim('sub', jwt['sub']))

        return principals


