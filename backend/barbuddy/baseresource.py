from barbuddy.basecontext import BaseContext


class BaseResource(object):

    def __init__(self, request, context: BaseContext = None):
        self.request = request
        self.context = context
        self.db = request.db
