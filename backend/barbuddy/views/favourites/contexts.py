import logging

from barbuddy import models as m
from barbuddy.basecontext import BaseContext

log = logging.getLogger(__name__)


class FavouriteContext(BaseContext):

    def favourites(self):
        user_id = self.request.authenticated_userid
        if not user_id:
            return []
        return [row[0] for row in self.db.query(m.FavRecipe.recipe_id).filter(
            m.FavRecipe.user_id == user_id
        )]

