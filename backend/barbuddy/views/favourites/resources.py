import logging

from barbuddy import models as m
from barbuddy.baseresource import BaseResource
from barbuddy.auth import Authenticated
from cornice.resource import resource, view

from .contexts import FavouriteContext

log = logging.getLogger(__name__)


@resource(
    path='/api/favourites',
    renderer='json',
    factory=FavouriteContext
)
class FavouriteResource(BaseResource):

    def __init__(self, request, context: FavouriteContext):
        super().__init__(request, context)

    def get(self):
        return {'data': self.context.favourites()}

    @view(permission=Authenticated())
    def put(self):
        new_favs = self.request.json_body

        self.db.query(m.FavRecipe).filter(
            m.FavRecipe.recipe_id.notin_(new_favs),
            m.FavRecipe.user_id == self.request.authenticated_userid
        ).delete(synchronize_session=False)

        for recipe_id in new_favs:
            self.db.merge(m.FavRecipe(
                user_id=self.request.authenticated_userid,
                recipe_id=recipe_id
            ))
        return {'data': new_favs}
