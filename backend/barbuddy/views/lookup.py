from barbuddy.baseresource import BaseResource
from cornice.resource import resource
from barbuddy import models as m


@resource(path="/api/catalogs")
class CatalogResource(BaseResource):
    def get(self):
        data = self.request.db.query(m.Catalog.catalog).order_by(m.Catalog.catalog)
        data = [row[0] for row in data]
        return {"data": data}


@resource(path="/api/categories")
class CategoryResource(BaseResource):
    def get(self):
        data = self.request.db.query(m.RecipeCategory.category).order_by(
            m.RecipeCategory.category
        )
        data = [row[0] for row in data]
        return {"data": data}


@resource(path="/api/vessels")
class VesselResource(BaseResource):
    def get(self):
        data = self.request.db.query(m.Vessel.vessel).order_by(m.Vessel.vessel)
        data = [row[0] for row in data]
        return {"data": data}


@resource(path="/api/ingredients")
class IngredientResource(BaseResource):
    def get(self):
        data = self.request.db.query(m.Ingredient).order_by(
            m.Ingredient.ingredient_name
        )
        return {"data": data}
