from cornice import Service
from barbuddy.auth import IsAdmin


auth_svc = Service(name='auth', path='/auth')


@auth_svc.get()
def get_auth(request):
    permission_keys = set()

    if request.has_permission(IsAdmin()):
        permission_keys.add('admin')

    return {'data': permission_keys}
