import logging

from barbuddy import models as m
from barbuddy.auth import Authenticated
from barbuddy.baseresource import BaseResource
from cornice.resource import resource, view

from .contexts import PantryContext

log = logging.getLogger(__name__)


@resource(
    path='/api/pantry',
    renderer='json',
    factory=PantryContext
)
class PantryResource(BaseResource):

    def __init__(self, request, context: PantryContext):
        super().__init__(request, context)

    def get(self):
        return {'data': self.context.pantry()}

    @view(permission=Authenticated())
    def put(self):
        # TODO: validate input
        new_pantry_items = self.request.json_body

        self.db.query(m.Pantry).filter(
            m.Pantry.ingredient_name.notin_(new_pantry_items),
            m.Pantry.user_id == self.request.authenticated_userid
        ).delete(synchronize_session=False)

        for ingredient_name in new_pantry_items:
            self.db.merge(m.Pantry(
                user_id=self.request.authenticated_userid,
                ingredient_name=ingredient_name
            ))
        return {'data': new_pantry_items}
