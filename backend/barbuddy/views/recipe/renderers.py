import logging
from barbuddy.auth import IsAdmin
from barbuddy.renderers import json_adapter, adapt_model
from barbuddy import models as m

log = logging.getLogger(__name__)


@json_adapter(m.Recipe)
def adapt_recipe(recipe, request):
    exclude = ['inserted_at', 'user_id', 'photo_path']
    include = ('recipe_ingredients', 'thumbnail_url', 'photo_url')

    if not request.has_permission(IsAdmin()):
        exclude.append('active')

    _add_photo_urls(recipe, request)

    return adapt_model(recipe, request, exclude, include)


@json_adapter(m.Ingredient)
def adapt_ingredient(ing, request):
    include = ('ingredient_name', 'parent_ingredient_name', 'description')
    exclude = ('approved',)
    return adapt_model(ing, request, exclude, include)


def _add_photo_urls(recipe, request):
    """
    Adds an attributes thumbnail_url and photo_url to recipe if the recipe has an image.
    """
    recipe_id = recipe.recipe_id
    images = recipe.recipe_images
    # Currently using the same image for main and thumbnail photo
    image_type = 'main'
    image = [image for image in images if image.image_type == image_type]

    if image:
        recipe.thumbnail_url = recipe.photo_url = request.route_path(
            'image',
            recipe_id=recipe_id,
            image_type=image_type
        )


@json_adapter(m.RecipeIngredient)
def adapt_recipe_ingredient(recipe_ingredient, request):
    return adapt_model(recipe_ingredient, request, exclude=('recipe_id',))
