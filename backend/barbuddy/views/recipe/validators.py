from barbuddy import models as m
from barbuddy.auth import IsAdmin
from cerberus import Validator


def build_schema(request):

    schema = {
        'recipe_name': {'type': 'string', 'required': True, },
        'description': {'type': 'string'},
        'category': {
            'type': 'string', 'allowed': options(request, m.RecipeCategory.category)
        },
        'directions': {'type': 'string', 'required': True},
        'story': {'type': 'string'},
        'vessel': {'type': 'string', 'allowed': options(request, m.Vessel.vessel)},
        'recipe_ingredients': {
            'type': 'list',
            'items': {
                'type': 'dict',
                'schema': {
                    'ingredient_name': {'type': 'string', 'required': True},
                    'amount': {'type': 'float'},
                    'unit': {'type': 'string', 'allowed': options(request, m.Unit.unit)},
                    'optional': {'type': 'bool', 'required': True},
                    'garnish': {'type': 'bool', 'required': True}
                }
            }
        }
    }

    if request.has_permission(IsAdmin()):
        schema['active'] = {
            'type': 'boolean'
        }

    return schema


def options(request, column):
    return request.db.query(column).all()


def validate_recipe(request):
    recipe = request.json_body

    schema = build_schema(request)
    validator = Validator(schema)
    valid = validator.validate(recipe)

    if not valid:
        for key, messages in validator.errors:
            for message in messages:
                request.errors.add('body', key, message)
