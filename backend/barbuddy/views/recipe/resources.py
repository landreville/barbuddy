import logging
from barbuddy import models as m
from barbuddy.auth import IsAdmin
from barbuddy.baseresource import BaseResource
from barbuddy.constants import vessel_images
from barbuddy.sync import sync_recipe
from barbuddy.views.recipe.contexts import RecipeContext, BaseRecipeContext
from cornice import Service
from cornice.resource import resource, view
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response


log = logging.getLogger(__name__)


@resource(
    collection_path="/api/recipes",
    path="/api/recipes/{recipe_id:\d+}",
    renderer="json",
    factory=RecipeContext,
)
class RecipeResource(BaseResource):
    def __init__(self, request, context: BaseRecipeContext):
        super().__init__(request, context)

    def collection_get(self):
        recipes = self.context.recipes()

        def gen_recipes():
            for recipe in recipes:
                recipe_ingredients = self._parse_recipe_ingredients(recipe)
                thumbnail_url = self._parse_thumbnail_url(recipe)

                recipe = {
                    "recipeId": recipe.recipe_id,
                    "recipeName": recipe.recipe_name,
                    "vessel": recipe.vessel,
                    "recipeIngredients": recipe_ingredients,
                    "catalog": recipe.catalog,
                    "thumbnailUrl": thumbnail_url,
                }
                yield recipe

        return {"data": gen_recipes()}

    def collection_post(self):
        sync_recipe(self.db, m.Recipe(), self.request.json_body)
        return {"data": {"success": True}}

    def get(self):
        recipe = self.context.recipe()
        return {"data": recipe}

    @view(permission=IsAdmin())
    def put(self):
        recipe = self.context.recipe()
        sync_recipe(self.db, recipe, self.request.json_body)
        log.info(f'Save recipe "{recipe.recipe_name}".')
        return {"data": {"success": True}}

    def _parse_recipe_ingredients(self, recipe):
        recipe_ingredients = [
            {
                "ingredientName": ri.ingredient_name,
                "optional": ri.optional,
                "garnish": ri.garnish,
                "parentIngredientName": ri.ingredient.parent_ingredient_name,
            }
            for ri in recipe.recipe_ingredients
        ]
        return recipe_ingredients

    def _parse_thumbnail_url(self, recipe):
        thumbnail_url = None
        if recipe.recipe_images:
            thumbnail_url = self.request.route_path(
                "image",
                recipe_id=recipe.recipe_id,
                image_type=recipe.recipe_images[0].image_type,
            )
        return thumbnail_url


image_svc = Service("image", "/api/recipes/{recipe_id}/image/{image_type}")


@image_svc.get()
def recipe_image(request):
    recipe_id = request.matchdict["recipe_id"]
    ri = (
        request.db.query(m.RecipeImage)
        .filter_by(recipe_id=recipe_id, image_type=request.matchdict["image_type"])
        .one_or_none()
    )

    if ri:
        return Response(ri.image, content_type=ri.mime_type)

    vessel = request.db.query(m.Recipe.vessel).filter_by(recipe_id=recipe_id).scalar()
    image_url = vessel_images.get(vessel, "/images/vessel/coupe.png")
    return HTTPFound(image_url)
