import logging

from barbuddy import models as m
from barbuddy.auth import IsAdmin
from barbuddy.basecontext import BaseContext
from pyramid.httpexceptions import HTTPNotFound
from sqlalchemy import or_, func, distinct, desc, case
from sqlalchemy.orm import joinedload, aliased, contains_eager

log = logging.getLogger(__name__)


class BaseRecipeContext(BaseContext):
    def __init__(self, request):
        super().__init__(request)
        self.limit = self.params.pop("limit", None)
        self.offset = self.params.pop("offset", None)

    def recipes(self):
        """
        Return query of recipes.

        Applies search parameters, limit, and offset.

        Limit and offset cannot be used in conjunction with search parameters.
        """
        recipes = (
            self.db.query(m.Recipe)
            .join(m.RecipeIngredient)
            .join(m.Ingredient)
            .outerjoin(m.RecipeImage)
            .options(
                contains_eager(m.Recipe.recipe_ingredients).contains_eager(
                    m.RecipeIngredient.ingredient
                ),
                contains_eager(m.Recipe.recipe_images),
            )
            .order_by(
                m.Recipe.recipe_name,
                case([(m.RecipeImage.image_type == "thumbnail", 0)], else_=1),
                case([(m.RecipeIngredient.unit == "ounce", 0)], else_=1),
                desc(m.RecipeIngredient.amount),
                m.RecipeIngredient.ingredient_name,
            )
        )

        if self.params:
            recipes = self._search_recipes(recipes)

            if self.limit is not None or self.offset is not None:
                raise ValueError(
                    "Limit and offset cannot be used with search parameters."
                )

        if self.limit is not None:
            recipes.limit(self.limit)

        if self.offset is not None:
            recipes.offset(self.offset)

        return recipes

    def recipe(self):
        recipe = (
            self.db.query(m.Recipe)
            .options(joinedload(m.Recipe.recipe_ingredients))
            .get(self.route_params["recipe_id"])
        )
        if not recipe:
            raise HTTPNotFound
        return recipe

    def _search_recipes(self, query):
        if self.params.get("q"):
            query = self._apply_basic_query(query)
        if self.params.get("ingredient[]"):
            query = self._apply_ingredients_query(query)
        if self.params.get("pantry[]"):
            query = self._apply_pantry_query(query)
        return query

    def _apply_basic_query(self, query):
        q = "%{}%".format(self.params["q"])
        recipe_ing = aliased(m.RecipeIngredient)
        ing = aliased(m.Ingredient)

        riq = (
            self.db.query(recipe_ing.recipe_id)
            .join(ing)
            .filter(
                or_(
                    recipe_ing.ingredient_name.ilike(q),
                    ing.parent_ingredient_name.ilike(q),
                )
            )
            .subquery()
        )

        return query.filter(
            or_(m.Recipe.recipe_name.ilike(q), m.Recipe.recipe_id.in_(riq))
        )

    def _apply_ingredients_query(self, query):
        ing_names = self.params["ingredient[]"]
        child_ing = aliased(m.Ingredient)
        parent_ing = aliased(m.Ingredient)

        riq = (
            self.db.query(m.RecipeIngredient.recipe_id)
            .join(
                m.Ingredient,
                m.RecipeIngredient.ingredient_name == m.Ingredient.ingredient_name,
            )
            .outerjoin(
                m.Substitute,
                m.Ingredient.ingredient_name == m.Substitute.ingredient_name,
            )
            .outerjoin(
                child_ing,
                m.Ingredient.ingredient_name == child_ing.parent_ingredient_name,
            )
            .outerjoin(
                parent_ing,
                m.Ingredient.parent_ingredient_name == parent_ing.ingredient_name,
            )
            .filter(
                or_(
                    m.Ingredient.ingredient_name.in_(ing_names),
                    child_ing.ingredient_name.in_(ing_names),
                    m.Substitute.ingredient_name.in_(ing_names),
                    parent_ing.ingredient_name.in_(ing_names),
                )
            )
            .group_by(m.RecipeIngredient.recipe_id)
            .having(
                func.count(distinct(m.RecipeIngredient.ingredient_name))
                == len(ing_names)
            )
        ).subquery()

        return query.join(riq, m.RecipeIngredient.recipe_id == riq.c.recipe_id)

    def _apply_pantry_query(self, query):
        pantry_items = self.params["pantry[]"]

        sibling_ing = aliased(m.Ingredient)
        count_ing = func.count(distinct(m.RecipeIngredient.ingredient_name))
        subq = (
            self.db.query(
                m.RecipeIngredient.recipe_id,
                count_ing.label("ingredient_count"),
                count_ing.filter(
                    or_(
                        m.RecipeIngredient.ingredient_name.in_(pantry_items),
                        m.Ingredient.parent_ingredient_name.in_(pantry_items),
                        sibling_ing.ingredient_name.in_(pantry_items),
                        m.Substitute.ingredient_name.in_(pantry_items),
                    )
                ).label("ingredient_matches"),
            )
            .select_from(m.RecipeIngredient)
            .join(
                m.Ingredient,
                m.RecipeIngredient.ingredient_name == m.Ingredient.ingredient_name,
            )
            .outerjoin(
                m.Substitute,
                m.Ingredient.ingredient_name == m.Substitute.ingredient_name,
            )
            .outerjoin(
                sibling_ing,
                sibling_ing.parent_ingredient_name
                == m.Ingredient.parent_ingredient_name,
            )
            .filter(
                m.RecipeIngredient.optional == False,
                m.RecipeIngredient.garnish == False,
            )
            .group_by(m.RecipeIngredient.recipe_id)
        ).subquery()

        riq = (
            self.db.query(subq).filter(
                (subq.c.ingredient_count - subq.c.ingredient_matches) <= 2
            )
        ).subquery()

        query = (
            query.join(riq, riq.c.recipe_id == m.RecipeIngredient.recipe_id)
            .order_by(None)
            .order_by(
                riq.c.ingredient_count - riq.c.ingredient_matches,
                m.RecipeIngredient.recipe_id,
            )
        )
        return query


class RecipeContext(BaseRecipeContext):
    def recipes(self):
        query = super().recipes()
        if not self.request.has_permission(IsAdmin()):
            query = query.filter(m.Recipe.active == True)
        return query

    def recipe(self):
        recipe = super().recipe()
        if not recipe.active and not self.request.has_permission(IsAdmin()):
            log.debug(f"Recipe is not active. Recipe ID: {recipe.recipe_id}")
            raise HTTPNotFound
        return recipe
