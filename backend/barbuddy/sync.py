import logging
from barbuddy import models as m

log = logging.getLogger(__name__)


def _to_snake_case(name):
    new_name = []
    for ch in name:
        if ch.isupper():
            new_name.append("_")
        new_name.append(ch)
    return "".join(ch).lower()


def sync_recipe(db, recipe, params):
    allowed = ("recipeName", "description", "category", "directions", "story", "vessel")

    for key, value in params.items():
        if key in allowed:
            setattr(recipe, _to_snake_case(key), value)

    if "recipeIngredients" in params.keys():
        for ri_params in params["recipeIngredients"]:
            ing = db.query(m.Ingredient).get(ri_params["ingredientName"])
            if not ing:
                ing = m.Ingredient(ri_params["ingredientName"])
            db.add(ing)

            ri = db.query(m.RecipeIngredient).get(
                (params["recipeId"], ri_params["ingredientName"])
            )
            if not ri:
                ri = m.RecipeIngredient(
                    recipe_id=recipe.recipe_id,
                    ingredient_name=ri_params["ingredientName"],
                )

            ri.amount = ri_params["amount"]
            ri.unit = ri_params["unit"]
            ri.optional = ri_params["optional"]
            ri.garnish = ri_params["garnish"]
            db.add(ri)

    ing_names = {ri_param["ingredientName"] for ri_param in params["recipeIngredients"]}

    new_ri = [ri for ri in recipe.recipe_ingredients if ri.ingredient_name in ing_names]
    recipe.recipe_ingredients = new_ri

    db.add(recipe)
    return recipe
