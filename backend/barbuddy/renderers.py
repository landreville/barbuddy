from datetime import datetime, timedelta
from decimal import Decimal
import logging
from types import GeneratorType

import venusian
from pyramid.renderers import JSON
from sqlalchemy.orm.query import Query
from barbuddy.models import Base


log = logging.getLogger(__name__)
json_renderer = JSON()


def includeme(config):
    config.add_renderer('json', json_renderer)


class json_adapter(object):

    def __init__(self, adapt_type):
        self.adapt_type = adapt_type

    def __call__(self, wrapped):
        venusian.attach(wrapped, self._register)
        return wrapped

    def _register(self, scanner, name, wrapped):
        log.debug(f'Registering JSON adapter for type: {self.adapt_type}')
        json_renderer.add_adapter(self.adapt_type, wrapped)


def _to_camelcase(snake):
    parts = snake.split('_')
    return parts[0] + ''.join(p.title() for p in parts[1:])


@json_adapter(dict)
def adapt_dict(dict_value, request):
    return {_to_camelcase(key): value for key, value in dict_value.items()}


@json_adapter(GeneratorType)
def adapt_generator(gen, request):
    return list(gen)


@json_adapter(datetime)
def adapt_datetime(dt, request):
    return dt.isoformat()


@json_adapter(Decimal)
def adapt_decimal(value, request):
    return float(value)


@json_adapter(Base)
def adapt_model(model_instance, request, exclude=(), include=()):
    return adapt_dict({name: getattr(model_instance, name, None)
            for name in include + tuple(c.name for c in model_instance.__table__.columns)
            if name not in exclude}, request)


@json_adapter(Query)
def adapt_query(query, request):
    return query.all()


@json_adapter(timedelta)
def adapt_timedelta(value, request):
    return round(value.total_seconds(), 2)


def adapt_entities(entities):
    for entity, args in entities.items():
        adapt_entity(entity, *args)


def adapt_entity(entity, exclude=(), include=()):
    json_renderer.add_adapter(
        entity,
        lambda value, request: adapt_model(value, request, exclude, include)
    )


