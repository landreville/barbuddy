from pyramid.config import Configurator


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include('cornice')
    config.include('barbuddy.auth')
    config.include('barbuddy.models')
    config.include('barbuddy.renderers')
    config.add_static_view('images', 'static/images')
    config.scan('barbuddy')
    return config.make_wsgi_app()
