class BaseContext(object):

    def __init__(self, request):
        self.request = request
        self.db = request.db
        params = {}
        params.update(request.params.mixed())
        for key, value in params.items():
            if key[-2:] == '[]' and not isinstance(value, list):
                params[key] = [value]
        self.params = params
        self.route_params = request.matchdict
