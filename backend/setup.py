from setuptools import setup, find_packages

setup(
    name='barbuddy',
    version='1.3.0',
    description='Bar Buddy',
    classifiers=[
        "Programming Language :: Python", "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application"
    ],
    keywords="web services",
    author='',
    author_email='',
    url='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'cerberus',
        'cornice',
        'inflect',
        'pendulum',
        'psycopg2-binary',
        'python-jose[pycryptodome]',
        'pyramid_tm',
        'requests',
        'sqlalchemy',
        'waitress',
        'zope.sqlalchemy',
    ],
    entry_points="""\
      [paste.app_factory]
      main=barbuddy:main
      """,
    paster_plugins=['pyramid']
)
